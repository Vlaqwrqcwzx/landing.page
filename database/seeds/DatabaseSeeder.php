<?php

use Illuminate\Database\Seeder;
use App\Models\Page;
use App\Models\Service;
use App\Models\Portfolio;
use App\Models\People;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(PeopleSeeder::class);
        $this->call(PortfolioSeeder::class);
        $this->call(ServiceSeeder::class);
    }
}
