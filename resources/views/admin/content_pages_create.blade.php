<div class="container">
    {!! Form::open(['url'=>route('pages.store'), 'class'=>'form-horizontal', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
        {!! Form::label('name', 'Название', ['class' => 'col-xs-2 control-label']) !!}
        <div class="col-zs-8">
            {{--name, value хелпер old--}}
            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Введите название страницы']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('alias', 'Псевдоним', ['class' => 'col-xs-2 control-label']) !!}
        <div class="col-zs-8">
            {{--name, value хелпер old--}}
            {!! Form::text('alias', old('alias'), ['class' => 'form-control', 'placeholder' => 'Введите псведоним страницы']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('text', 'Текст', ['class' => 'col-xs-2 control-label']) !!}
        <div class="col-zs-8">
            {{--name, value хелпер old--}}
            <textarea name="text" id="editor" cols="30" rows="10" class="form-control" placeholder='Введите текств страницы'></textarea>
{{--            {!! Form::text('text', old('text'), ['id' => 'editor', 'class' => 'form-control', 'placeholder' => 'Введите текств страницы']) !!}--}}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('images', 'Изображение:',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::file('images', ['class' => 'filestyle','data-buttonText'=>'Выберите изображение','data-buttonName'=>"btn-primary",'data-placeholder'=>"Файла нет"]) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('Сохранить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>

    {!! Form::close() !!}


</div>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('editor');
    });
</script>