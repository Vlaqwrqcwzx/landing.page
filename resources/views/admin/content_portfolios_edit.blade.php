<div class="container">
    {!! Form::open(['url'=>route('portfolios.update', $portfolio), 'class'=>'form-horizontal', 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
        {!! Form::label('name', 'Название', ['class' => 'col-xs-2 control-label']) !!}
        <div class="col-zs-8">
            {{--name, value хелпер old--}}
            {!! Form::text('name', $portfolio->name, ['class' => 'form-control', 'placeholder' => 'Введите название страницы']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('filter', 'Фильтр', ['class' => 'col-xs-2 control-label']) !!}
        <div class="col-zs-8">
            {{--name, value хелпер old--}}
            {!! Form::text('filter', $portfolio->filter, ['class' => 'form-control', 'placeholder' => 'Введите название страницы']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('old_images', 'Старое изображение:',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Html::image('assets/img/'.$portfolio->images, '', ['class'=>'img-circle img-responsive','width'=>'150px'] ) !!}
            {!! Form::hidden('old_images', $portfolio->images) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('images', 'Обновить изображение:',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::file('images', ['class' => 'filestyle','data-buttonText'=>'Выберите изображение','data-buttonName'=>"btn-primary",'data-placeholder'=>"Файла нет"]) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('Обновить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>

    {!! Form::close() !!}


</div>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('editor');
    });
</script>