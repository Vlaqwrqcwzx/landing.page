<div class="container">
    @if($portfolios)
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Фильтр</th>
                <th>Дата создания</th>
                <th>Удалить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($portfolios as $k => $portfolio)
                <tr>
                    <td>{{ $portfolio->id }}</td>
                    <td>{!! Html::link(route('portfolios.edit',$portfolio), $portfolio->name, ['alt' => $portfolio->name]) !!}</td>
                    <td>{{ $portfolio->filter }}</td>
                    <td>{{ $portfolio->created_at }}</td>
                    <td>
                        {!! Form::open(['url'=>route('portfolios.destroy',$portfolio), 'class'=>'form-horizontal', 'method' => 'POST']) !!}
                        {{--name=action value=delete--}}
                        {!! Form::hidden('_method', 'delete') !!}
                        {{--value , --}}
                        {!! Form::button('Удалить', ['class' => 'btn btn-danger', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    {!! Html::link(route('portfolios.create'), 'Новая запись', ['class' => 'btn btn-success']) !!}
</div>