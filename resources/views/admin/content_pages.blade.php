<div class="container">
    @if($pages)
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Псевдоним</th>
                <th>Текст</th>
                <th>Дата создания</th>
                <th>Удалить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pages as $k => $page)
                <tr>
                    <td>{{ $page->id }}</td>
                    <td>{!! Html::link(route('pages.edit',$page), $page->name, ['alt' => $page->name]) !!}</td>
                    <td>{{ $page->alias }}</td>
                    <td>{{ $page->text }}</td>
                    <td>{{ $page->created_at }}</td>
                    <td>
                        {!! Form::open(['url'=>route('pages.destroy',$page), 'class'=>'form-horizontal', 'method' => 'POST']) !!}
                        {{--name=action value=delete--}}
                        {!! Form::hidden('_method', 'delete') !!}
                        {{--value , --}}
                        {!! Form::button('Удалить', ['class' => 'btn btn-danger', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    {!! Html::link(route('pages.create'), 'Новая страница', ['class' => 'btn btn-success']) !!}
</div>