<div class="container">
    {!! Form::open(['url'=>route('pages.update',$page), 'class'=>'form-horizontal', 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
        {!! Form::label('name', 'Название', ['class' => 'col-xs-2 control-label']) !!}
        <div class="col-zs-8">
            {{--name, value хелпер old--}}
            {!! Form::text('name', $page->name, ['class' => 'form-control', 'placeholder' => 'Введите название страницы']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('alias', 'Псевдоним', ['class' => 'col-xs-2 control-label']) !!}
        <div class="col-zs-8">
            {{--name, value хелпер old--}}
            {!! Form::text('alias', $page->alias, ['class' => 'form-control', 'placeholder' => 'Введите псведоним страницы']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('text', 'Текст', ['class' => 'col-xs-2 control-label']) !!}
        <div class="col-zs-8">
            {{--name, value хелпер old--}}
            <textarea name="text" id="editor" cols="30" rows="10" class="form-control" placeholder='Введите текств страницы'>{{ $page->text }}</textarea>
            {{--            {!! Form::text('text', old('text'), ['id' => 'editor', 'class' => 'form-control', 'placeholder' => 'Введите текств страницы']) !!}--}}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('old_images', 'Старое изображение:',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Html::image('assets/img/'.$page->images, '', ['class'=>'img-circle img-responsive','width'=>'150px']) !!}
            {!! Form::hidden('old_images', $page->images) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('images', 'Обновить изображение:',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::file('images', ['class' => 'filestyle','data-buttonText'=>'Выберите изображение','data-buttonName'=>"btn-primary",'data-placeholder'=>"Файла нет"]) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('Обновить', ['class' => 'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>

    {!! Form::close() !!}


</div>
<script>
    $(document).ready(function () {
        CKEDITOR.replace('editor');
    });
</script>