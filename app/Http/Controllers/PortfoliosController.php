<?php

namespace App\Http\Controllers;

use App\Models\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PortfoliosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (view()->exists('admin.portfolios')) {
            $portfolios = Portfolio::all();
            $data = [
                'title' => 'Страницы',
                'portfolios' => $portfolios
            ];
            return view('admin.portfolios')->with($data);
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (view()->exists('admin.portfolios_create')) {
            $data = [
                'title' => 'Новая запись',
            ];
            return view('admin.portfolios_create')->with($data);
        } else {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $messages = [
            'required' => 'Поле :attribute обязательно к заполнению',
        ];
        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'filter' => 'required|max:255',
            'images' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($input);
        }
        if ($request->hasFile('images')) {
            $file = $request->file('images');
            $input['images'] = $file->getClientOriginalName();
            $file->move(public_path() . '/assets/img', $input['images']);
        }
        $portfolio = Portfolio::create($input);

        if ($portfolio) {
            return redirect()->route('portfolios.index')->with('status', 'Запись добавлена');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Portfolio $portfolio)
    {
        if (view()->exists('admin.portfolios_edit')) {
            $data = [
                'title' => 'Рудактирование портфолио - ' . $portfolio->name,
                'portfolio' => $portfolio
            ];
            return view('admin.portfolios_edit')->with($data);
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Portfolio $portfolio)
    {
        $input = $request->except('_token', '_method');
        $messages = [
            'required' => 'Поле :attribute обязательно к заполнению',
        ];
        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'filter' => 'required|max:255'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($input);
        }

        if ($request->hasFile('images')) {
            $file = $request->file('images');
            $input['images'] = $file->getClientOriginalName();
            $file->move(public_path() . '/assets/img', $input['images']);
        } else {
            $input['images'] = $input['old_images'];
        }
        unset($input['old_images']);
        $portfolio->update($input);
        return redirect()->route('portfolios.index')->with('status', 'Запись обновленна');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portfolio $portfolio)
    {
        $portfolio->delete();
        return redirect()->route('portfolios.index')->with('status', 'Запись удалена');
    }
}
