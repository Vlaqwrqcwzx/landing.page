<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\People;
use App\Models\Portfolio;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class IndexController extends Controller
{
    public function execute(Request $request)
    {
        if ($request->isMethod('post')) {
            $messages = [
                'required' => 'Поле :attribute обязательно к заполнению',
                'email' => 'Поле :attribute должно соответствовать email адресу',
            ];
            $this->validate($request, [
                'name' => 'required|max:255',
                'email' => 'required|email',
                'text' => 'required'
            ], $messages);

            $data = $request->all();

            // mail
            $result = Mail::send('site.email', ['data'=>$data], function ($message) use ($data) {
                $mail_admin = env('MAIL_ADMIN');

                $message->from($data['email'], $data['name']);
                $message->to($mail_admin)->subject('Theme message/ Question');
            });

            if ($result) {
                return redirect()->route('home')->with('mail_status', 'Email is send'); // значение в сессии
            }
        }

        $data['menu'] = Page::get(['name', 'alias'])->toArray();
        $data['pages'] = Page::all();
        $data['portfolios'] = Portfolio::get(['name', 'filter', 'images']);
        $data['services'] = Service::all();
        $data['peoples'] = People::take(3)->get();

        $data['tags'] = DB::table('portfolios')->distinct()->pluck('filter');

        array_push($data['menu'], ['name' => 'Service', 'alias' => 'service']);
        array_push($data['menu'], ['name' => 'Portfolio', 'alias' => 'Portfolio']);
        array_push($data['menu'], ['name' => 'Team', 'alias' => 'team']);
        array_push($data['menu'], ['name' => 'Contact', 'alias' => 'contact']);

        return view('site.index')->with($data);
    }
}
