<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{
    public function index()
    {
        if (view()->exists('admin.pages')) {
            $pages = Page::all();
            $data = [
                'title' => 'Страницы',
                'pages' => $pages
            ];
            return view('admin.pages')->with($data);
        } else {
            abort(404);
        }
    }

    public function create()
    {
        if (view()->exists('admin.pages_create')) {
            $data = [
                'title' => 'Новая страница',
            ];
            return view('admin.pages_create')->with($data);
        } else {
            abort(404);
        }
    }

    public function store(Request $request)
    {
        $input = $request->except('_token');

        $messages = [
            'required' => 'Поле :attribute обязательно к заполнению',
        ];
        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'alias' => 'required|unique:pages|max:255',
            'text' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($input);
        }

        if ($request->hasFile('images')) {
            $file = $request->file('images');
            $input['images'] = $file->getClientOriginalName();
            $file->move(public_path() . '/assets/img', $input['images']);
        }

        $page = Page::create($input);

        if ($page) {
            return redirect()->route('pages.index')->with('status', 'Страница добавлена');
        }
    }

    public function edit(Page $page)
    {
        if (view()->exists('admin.pages_edit')) {
            $data = [
                'title' => 'Рудактирование страницы - ' . $page->name,
                'page' => $page
            ];
            return view('admin.pages_edit')->with($data);
        } else {
            abort(404);
        }
    }

    public function update(Request $request, Page $page)
    {
        $input = $request->except('_token', '_method');
        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'alias' => 'required|max:255|unique:pages,alias,' . $page->id,
            'text' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($input);
        }

        if ($request->hasFile('images')) {
            $file = $request->file('images');
            $input['images'] = $file->getClientOriginalName();
            $file->move(public_path() . '/assets/img', $input['images']);
        } else {
            $input['images'] = $input['old_images'];
        }
        unset($input['old_images']);
        $page->update($input);
        return redirect()->route('pages.index')->with('status', 'Страница обновленна');
    }

    public function destroy(Page $page)
    {
        $page->delete();
        return redirect()->route('pages.index')->with('status', 'Страница удалена');
    }

}
