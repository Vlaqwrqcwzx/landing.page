<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

- [Demo version](http://still-castle-79870.herokuapp.com/).

## Использованные команды
<p>composer require laravelcollective/html</p>

<p>php artisan make:migration create_table_pages 		--create=pages</p>
<p>php artisan make:migration create_table_services 	--create=services</p>
<p>php artisan make:migration create_table_portfolios 	--create=portfolios</p>
<p>php artisan make:migration create_table_peoples 	--create=peoples</p>
<p>php artisan migrate</p>

<p>php artisan make:model Models\Page</p>
<p>php artisan make:model Models\Service</p>
<p>php artisan make:model Models\Portfolio</p>
<p>php artisan make:model Models\People</p>

<p>php artisan make:seeder PagesSeeder</p>
<p>php artisan make:seeder ServiceSeeder</p>
<p>php artisan make:seeder PortfolioSeeder</p>
<p>php artisan make:seeder PeopleSeeder</p>
<p>php artisan db:seed</p>

<p>php artisan make:controller IndexController</p>
<p>php artisan make:controller PageController</p>

<p>php artisan make:auth</p>

<p>php artisan make:controller PagesController</p>

<p>php artisan make:controller ServicesController --resource</p>
<p>php artisan make:controller PortfoliosController --resource</p>